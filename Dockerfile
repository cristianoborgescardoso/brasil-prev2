#FROM maven:3-jdk-8-alpine

#WORKDIR /usr/src/app

#COPY . /usr/src/app
#RUN mvn package

#ENV PORT 8080
#EXPOSE $PORT
#CMD [ "sh", "-c", "mvn -Dserver.port=${PORT} spring-boot:run" ]


FROM openjdk:11.0.4-jdk
#VOLUME /tmp
#RUN mvn package
WORKDIR /
COPY src/main/resources/ /app/
ADD /target/brasilprev*.jar /app/brasilprev.jar
ENTRYPOINT ["java","-jar","/app/brasilprev.jar"]	
EXPOSE 8080
