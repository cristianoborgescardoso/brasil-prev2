package br.com.brasilprev.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpClientErrorException;

import br.com.brasilprev.BrasilPrevApplication;
import br.com.brasilprev.model.CustomerModel;
import br.com.brasilprev.model.vo.CustomerVo;

//@RunWith(SpringRunner.class)
@SpringBootTest(classes = BrasilPrevApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class CustomerControllerIntegrationTest {
	Logger LOGGER = LogManager.getLogger();
	
	 private static final String _CUSTOMERS= "/customers";
     @Autowired
     private TestRestTemplate restTemplate;

     @LocalServerPort
     private int port;

     private String getRootUrl() {
         return "http://localhost:" + port;
     }
     
     private static CustomerModel customer;

     @Test
     public void stage1_contextLoads() {

     }

     @Test
     public void stage6_testGetAllEmployees() {
     HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(getRootUrl() + _CUSTOMERS,
        HttpMethod.GET, entity, String.class);        
        assertNotNull(response.getBody());
    }

    @Test
    public void stage3_testGetEmployeeById() {
    	LOGGER.info("testGetEmployeeById() START");
        customer = restTemplate.getForObject(getRootUrl() + _CUSTOMERS +"/"+customer.getCustomerId(), CustomerModel.class);
        LOGGER.info(customer);
        assertNotNull(customer);
        LOGGER.info("testGetEmployeeById() END");
    }

    @Test
    public void stage2_testCreateEmployee() {
    	 LOGGER.info("testCreateEmployee() START");
    	customer = new CustomerModel();
        customer.setCpfCnpj("111-222-333-44");
        customer.setName("Frodo Baggins");
        ResponseEntity<CustomerModel> postResponse = restTemplate.postForEntity(getRootUrl() + _CUSTOMERS, customer, CustomerModel.class);
        assertNotNull(postResponse); 
        customer = (postResponse.getBody());
        assertNotNull(postResponse.getBody());
        LOGGER.info("CREATED CUSTOMER[{}]",customer);       
   	 	LOGGER.info("testCreateEmployee() END");
    }

    @Test
    public void stage4_testUpdateEmployee() {
   	 LOGGER.info("testUpdateEmployee() START");
        customer = restTemplate.getForObject(getRootUrl() + _CUSTOMERS + "/" + customer.getCustomerId(), CustomerModel.class);
        customer.setCpfCnpj("555-666-777-88");
        customer.setName("Bilbo Baggins");
        LOGGER.info("testUpdateEmployee() CUSTOMER[{}]",customer);
        restTemplate.put(getRootUrl() + _CUSTOMERS, new CustomerVo(customer));
        CustomerModel updatedCustomer = restTemplate.getForObject(getRootUrl() + _CUSTOMERS +"/" + customer.getCustomerId(), CustomerModel.class);
        
        LOGGER.info("Comparing customer[{}] and updatedCustomer[{}]",customer,updatedCustomer);
        assertNotNull(updatedCustomer);
        assertEquals(customer,updatedCustomer);
        LOGGER.info("testUpdateEmployee() END");
    }

    @Test
    public void stage5_testDeleteEmployee() {
         customer = restTemplate.getForObject(getRootUrl() + _CUSTOMERS + "/" + customer.getCustomerId(), CustomerModel.class);
         assertNotNull(customer);
         restTemplate.delete(getRootUrl() + _CUSTOMERS + "/" +  customer.getCustomerId());
         try {
        	 customer = restTemplate.getForObject(getRootUrl() + _CUSTOMERS + "/" + customer.getCustomerId(), CustomerModel.class);
         } catch (final HttpClientErrorException e) {
              assertEquals(e.getStatusCode(), HttpStatus.NOT_FOUND);
         }
    }
}