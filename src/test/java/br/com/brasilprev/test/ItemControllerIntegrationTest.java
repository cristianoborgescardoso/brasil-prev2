package br.com.brasilprev.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpClientErrorException;

import br.com.brasilprev.BrasilPrevApplication;
import br.com.brasilprev.model.ItemModel;
import br.com.brasilprev.model.vo.ItemVo;

//@RunWith(SpringRunner.class)
@SpringBootTest(classes = BrasilPrevApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class ItemControllerIntegrationTest {
	Logger LOGGER = LogManager.getLogger();
	
	 private static final String _ITEMS= "/items";
     @Autowired
     private TestRestTemplate restTemplate;

     @LocalServerPort
     private int port;

     private String getRootUrl() {
         return "http://localhost:" + port;
     }
     
     private static ItemModel item;

     @Test
     public void stage1_contextLoads() {

     }

     @Test
     public void stage6_testGetAllEmployees() {
     HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(getRootUrl() + _ITEMS,
        HttpMethod.GET, entity, String.class);        
        assertNotNull(response.getBody());
    }

    @Test
    public void stage3_testGetEmployeeById() {
    	LOGGER.info("testGetEmployeeById() START");
        item = restTemplate.getForObject(getRootUrl() + _ITEMS +"/"+item.getItemId(), ItemModel.class);
        LOGGER.info(item);
        assertNotNull(item);
        LOGGER.info("testGetEmployeeById() END");
    }

    @Test
    public void stage2_testCreateEmployee() {
    	 LOGGER.info("testCreateEmployee() START");
    	item = new ItemModel();
        item.setPartNumber("111-222-333-44");
        item.setDescription("Frodo Baggins");
        item.setPrice(1F);
        ResponseEntity<ItemModel> postResponse = restTemplate.postForEntity(getRootUrl() + _ITEMS, item, ItemModel.class);
        assertNotNull(postResponse); 
        item = (postResponse.getBody());
        assertNotNull(postResponse.getBody());
        LOGGER.info("CREATED CUSTOMER[{}]",item);       
   	 	LOGGER.info("testCreateEmployee() END");
    }

    @Test
    public void stage4_testUpdateEmployee() {
   	 LOGGER.info("testUpdateEmployee() START");
        item = restTemplate.getForObject(getRootUrl() + _ITEMS + "/" + item.getItemId(), ItemModel.class);
        item.setPartNumber("555-666-777-88");
        item.setDescription("Bilbo Baggins");
        item.setPrice(2F);
        LOGGER.info("testUpdateEmployee() CUSTOMER[{}]",item);
        restTemplate.put(getRootUrl() + _ITEMS, item);
        ItemModel updatedItem = restTemplate.getForObject(getRootUrl() + _ITEMS +"/" + item.getItemId(), ItemModel.class);
        
        LOGGER.info("Comparing item[{}] and updatedItem[{}]",item,updatedItem);
        assertNotNull(updatedItem);
        assertEquals(item,updatedItem);
        LOGGER.info("testUpdateEmployee() END");
    }

    @Test
    public void stage5_testDeleteEmployee() {
         item = restTemplate.getForObject(getRootUrl() + _ITEMS + "/" + item.getItemId(), ItemModel.class);
         assertNotNull(item);
         restTemplate.delete(getRootUrl() + _ITEMS + "/" +  item.getItemId());
         try {
        	 item = restTemplate.getForObject(getRootUrl() + _ITEMS + "/" + item.getItemId(), ItemModel.class);
         } catch (final HttpClientErrorException e) {
              assertEquals(e.getStatusCode(), HttpStatus.NOT_FOUND);
         }
    }
}