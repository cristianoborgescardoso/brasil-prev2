package br.com.brasilprev;

import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.bind.annotation.RestController;

import br.com.brasilprev.util.DateUtils;

@SpringBootApplication
@RestController
@EnableAutoConfiguration
@ComponentScan(basePackages = {"br.com.brasilprev.","br.com.brasilprev.controller."}) // auto-referência é essencial para testes!
@EntityScan(basePackages = {"br.com.brasilprev.model."})  // scan JPA entities
@EnableJpaRepositories(basePackages = {"br.com.brasilprev.persistence.repository."}) 
public class BrasilPrevApplication {

	@PostConstruct
    void started() {
      TimeZone.setDefault(TimeZone.getTimeZone(DateUtils.TIME_ZONE));
    }
	
	public static void main(String[] args) {
		SpringApplication.run(BrasilPrevApplication.class, args);
	}
}
