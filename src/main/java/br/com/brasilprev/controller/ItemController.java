package br.com.brasilprev.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import br.com.brasilprev.model.ItemModel;
import br.com.brasilprev.model.vo.ItemVo;
import br.com.brasilprev.service.ItemService;

@RestController
@RequestMapping("/items")
public class ItemController {

	Logger LOGGER = LogManager.getLogger();

	@Autowired
	private ItemService itemService;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getItems() {

		try {
			List<ItemModel> itemsList = itemService.getItems();

			if (itemsList.isEmpty()) {
				return ResponseEntity.noContent().build();
			}
			return ResponseEntity.ok(itemsList);

		} catch (Exception e) {
			LOGGER.error("Error while listing Items.", e);
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
					String.format("Error while listing items: %s", e.getMessage()), e);
		}
	}

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> findById(@PathVariable Long id) {
		try {
			Optional<ItemModel> item = itemService.getById(id);

			if (item.isPresent()) {
				return ResponseEntity.ok(item.get());
			}
			return ResponseEntity.notFound().build();
		} catch (Exception e) {
			LOGGER.error("Error while get Item by Id[{}].", id, e);
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
					String.format("Error while get Item by Id[%d]: %s.", id, e.getMessage()), e);
		}
	}

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<?> add(@Valid @RequestBody ItemModel item) {
		try {
			Optional<ItemModel> foundItem = itemService.getByPartNumber(item.getPartNumber());
			if (foundItem.isPresent()) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
						String.format("There is already an item with this partNumber[%s]", item.getPartNumber()));
			}
			return ResponseEntity.ok(itemService.save(item));
		} catch (Exception e) {
			LOGGER.error("Error while add item[{}].", item, e);
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
					String.format("Error while add item[%s]: %s", item, e.getMessage()), e);
		}
	}	
	
	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ItemModel> update(@RequestBody ItemModel updatingItem) {
        Optional<ItemModel> itemOptional = itemService.getById(updatingItem.getItemId());
        if (!itemOptional.isPresent()) {
        	LOGGER.error(String.format("Does not exist a Item with the privided Id[%s].", updatingItem.getItemId()));
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
            		String.format("Does not exist a Item with the privided Id[%s].", updatingItem.getItemId()));
        }
        return ResponseEntity.accepted().body(itemService.update(itemOptional.get(),updatingItem));
    }
	
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remove(@PathVariable Long id) {
		itemService.deleteById(id);

	}

}
