package br.com.brasilprev.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import br.com.brasilprev.model.CustomerModel;
import br.com.brasilprev.model.vo.CustomerVo;
import br.com.brasilprev.service.CustomerService;

@RestController
@RequestMapping("/customers")
public class CustomerController {

	Logger LOGGER = LogManager.getLogger();

	@Autowired
	private CustomerService customerService;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getCustomers() {

		try {
			List<CustomerModel> customerList = customerService.getCustomers();

			if (customerList.isEmpty()) {
				return ResponseEntity.noContent().build();
			}

			return ResponseEntity.ok(customerList);

		} catch (Exception e) {
			LOGGER.error("Error while listing customers.", e);
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
					String.format("Error while listing customers: %s.", e.getMessage()), e);
		}
	}

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> findById(@PathVariable Long id) {
		try {
			Optional<CustomerModel> foundCustomer = customerService.getById(id);

			if (foundCustomer.isPresent()) {
				return ResponseEntity.ok(foundCustomer.get());
			}
			return ResponseEntity.notFound().build();
		} catch (Exception e) {
			LOGGER.error("Error while get customer by Id[{}].", id, e);
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
					String.format("Error while get customer by Id[%d]: %s.", id, e.getMessage()), e);
		}
	}

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<?> add(@Valid @RequestBody CustomerModel customer) {
		try {
			Optional<CustomerModel> foundCustomer = customerService.getByCpfCnpj(customer.getCpfCnpj());
			if (foundCustomer.isPresent()) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
						String.format("There is already an customer with this cpf_cnpj[%s]", customer.getCpfCnpj()));
			}
			return ResponseEntity.ok(customerService.save(customer));
		} catch (Exception e) {
			LOGGER.error("Error while add the customer[{}].", customer, e);
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
					String.format("Error while add the customer[%s]: %s.", customer, e.getMessage()), e);
		}
	}

	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CustomerModel> update(@RequestBody CustomerVo updatingCustomer) {
        Optional<CustomerModel> customerOptional = customerService.getById(updatingCustomer.getCustomerId());
        if (!customerOptional.isPresent()) {
        	LOGGER.error(String.format("Does not exist a Customer with the privided Id[%s].", updatingCustomer.getCustomerId()));
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
            		String.format("Does not exist a Customer with the privided Id[%s].", updatingCustomer.getCustomerId()));
        }
        return ResponseEntity.accepted().body(customerService.update(customerOptional.get(),updatingCustomer));
    }

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remove(@PathVariable Long id) {
		customerService.deleteById(id);
	}
}
