package br.com.brasilprev.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import br.com.brasilprev.model.PurchaseModel;
import br.com.brasilprev.model.vo.PurchaseVo;
import br.com.brasilprev.service.PurchaseService;
import javassist.NotFoundException;

@RestController
@RequestMapping("/purchases")
public class PurchaseController {

	Logger LOGGER = LogManager.getLogger();

	@Autowired
	private PurchaseService purchaseService;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getPurchases() {

		try {
			List<PurchaseVo> purchasesList = purchaseService.getPurchases();

			if (purchasesList.isEmpty()) {
				return ResponseEntity.noContent().build();
			}

			return ResponseEntity.ok(purchasesList);

		} catch (Exception e) {
			LOGGER.error("Error while listing purchases.", e);
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
					String.format("Error while listing purchases: %s", e.getMessage()), e);
		}
	}

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> find(@PathVariable Long id) {
		try {
			Optional<PurchaseModel> foundPurchase = purchaseService.getById(id);

			if (foundPurchase.isPresent()) {
				return ResponseEntity.ok(new PurchaseVo(foundPurchase.get()));
			}
			return ResponseEntity.notFound().build();
		} catch (Exception e) {
			LOGGER.error("Error while get purchase by Id[{}].", id, e);
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
					String.format("Error while get Item by Id[%d]: %s", id, e.getMessage()), e);
		}
	}

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<?> add(@Valid @RequestBody PurchaseVo purchaseVo) {
		try {
			return ResponseEntity.ok(new PurchaseVo(purchaseService.save(purchaseVo)));
		} catch (Exception e) {
			LOGGER.error("Error while add purchase[{}].", purchaseVo, e);
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
					String.format("Error while add purchase[%s]: %s.", purchaseVo, e.getMessage()), e);
		}
	}
	
	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PurchaseModel> update(@RequestBody PurchaseVo updatingPurchase) {
		try {
			Optional<PurchaseModel> purchaseOptional = purchaseService.getById(updatingPurchase.getPurchaseId());
			if (!purchaseOptional.isPresent()) {
				LOGGER.error(String.format("Does not exist a Purchase with the privided Id[%s].",
						updatingPurchase.getPurchaseId()));
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, String.format(
						"Does not exist a Purchase with the privided Id[%s].", updatingPurchase.getPurchaseId()));
			}
			return ResponseEntity.accepted().body(purchaseService.update(purchaseOptional.get(), updatingPurchase));
		} catch (Exception e) {
			LOGGER.error("Error while update purchase[{}].", updatingPurchase, e);
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
					String.format("Error while update purchase[%s]: %s.", updatingPurchase, e.getMessage()), e);
		}
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remove(@PathVariable Long id) {
		purchaseService.deleteById(id);
	}

}
