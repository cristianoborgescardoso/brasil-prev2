package br.com.brasilprev.service;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.brasilprev.model.CustomerModel;
import br.com.brasilprev.model.ItemModel;
import br.com.brasilprev.model.ItemPurchaseModel;
import br.com.brasilprev.model.PurchaseModel;
import br.com.brasilprev.model.vo.PurchaseVo;
import br.com.brasilprev.persistence.repository.PurchaseRepository;
import javassist.NotFoundException;

@Service
public class PurchaseService {
	
	@Autowired
	private PurchaseRepository purchaseRepository;
	
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private ItemService itemService;

	@Autowired
	private ItemPurchaseService itemPurchaseService;
	
	public List<PurchaseVo> getPurchases()
	{
		return purchaseRepository.findAll().parallelStream().map(purchaseModel -> new PurchaseVo(purchaseModel)).collect(Collectors.toList());
	}
	
	public Optional<PurchaseModel> getById(Long id)
	{
		return purchaseRepository.findById(id);
	}
		
	public PurchaseModel save(PurchaseModel purchase)
	{
		return purchaseRepository.save(purchase);
	}
	
	@Transactional(rollbackFor=Exception.class)
	public PurchaseModel save(PurchaseVo purchaseVo) throws NotFoundException
	{
		Optional<CustomerModel> customerOptional = customerService.getById(purchaseVo.getCustomerVo().getCustomerId());
		if (!customerOptional.isPresent()) 
		{
			throw new NotFoundException(String.format("Customer[%s] not Found.",purchaseVo.getCustomerVo().toString()));
		}
		PurchaseModel purchase = new PurchaseModel();
		
		purchase.setPurchaseTime(new Timestamp(System.currentTimeMillis()));
		
		purchase.setCustomer(customerOptional.get());
		
		purchase.setItemsPurchaseList(purchaseVo.getItemVoList().parallelStream().map(itemPurchaseVo -> 
			{
				ItemPurchaseModel itemPurchase = new ItemPurchaseModel();	
				Optional<ItemModel> itemOptional = itemService.getById(itemPurchaseVo.getItemId());
				if (!itemOptional.isPresent()) 
				{
					throw new RuntimeException(new NotFoundException(String.format("Item[%s] not Found.",itemPurchaseVo.toString())));
				}
				itemPurchase.setItem(itemOptional.get());
				itemPurchase.setCurrentPrice(itemOptional.get().getPrice());
				itemPurchase.setAmount(itemPurchaseVo.getAmount());
				itemPurchase.setPurchase(purchase);
				return itemPurchase;
			}).collect(Collectors.toList()));		
		
		return purchaseRepository.save(purchase);
	}
	
	@Transactional(rollbackFor=Exception.class)
	public PurchaseModel update(PurchaseModel purchase, PurchaseVo purchaseVo) throws NotFoundException
	{		
		purchase.setPurchaseTime(purchaseVo.getPurchaseTime());
		
		List<ItemPurchaseModel> toBeRemovedItemsPurchaseList = purchase.getItemsPurchaseList(); 
		
		purchase.setItemsPurchaseList(purchaseVo.getItemVoList().parallelStream().map(itemPurchaseVo -> 
			{
				ItemPurchaseModel itemPurchase;	
				Optional<ItemPurchaseModel> itemPurchaseOptional = itemPurchaseService.getByPurchaseIdAndItemId(purchase.getPurchaseId(),itemPurchaseVo.getItemId());
				Optional<ItemModel> itemOptional = itemService.getById(itemPurchaseVo.getItemId());
				
				//create if not exist
				if (!itemPurchaseOptional.isPresent()) 
				{
					itemPurchase = new ItemPurchaseModel();
					if(!itemOptional.isPresent())
					{
						throw new RuntimeException(new NotFoundException(String.format("Item[%s] not Found.",itemPurchaseVo.toString())));
					}
					itemPurchase.setItem(itemOptional.get());
					itemPurchase.setCurrentPrice(itemOptional.get().getPrice());
					itemPurchase.setAmount(itemPurchaseVo.getAmount());
					itemPurchase.setPurchase(purchase);
					
				}else
				{
					itemPurchase=itemPurchaseOptional.get();
					toBeRemovedItemsPurchaseList.remove(itemPurchase);
				}				
				return itemPurchase;
			}).collect(Collectors.toList()));
		
		toBeRemovedItemsPurchaseList.parallelStream().forEach(ip ->
		{
			//remove itens that not is present in purchaseVo
			purchase.getItemsPurchaseList().remove(ip);
		});
		
		return purchaseRepository.save(purchase);
	}
	
	
	public void deleteById(Long id)
	{
		purchaseRepository.deleteById(id);
	}	
}
