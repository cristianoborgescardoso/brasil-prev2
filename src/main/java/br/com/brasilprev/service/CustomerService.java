package br.com.brasilprev.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import br.com.brasilprev.model.CustomerModel;
import br.com.brasilprev.model.vo.CustomerVo;
import br.com.brasilprev.persistence.repository.CustomerRepository;

@Service
public class CustomerService {
	
	@Autowired
	private CustomerRepository customerRepository;
	
	public List<CustomerModel> getCustomers()
	{
		return customerRepository.findAll();
	}
	
	public Optional<CustomerModel> getById(Long id)
	{
		return customerRepository.findById(id);
	}
	
	public Optional<CustomerModel> getByCpfCnpj(String cpfCnpj)
	{
		return customerRepository.findByCpfCnpj(cpfCnpj);
	}
	
	public CustomerModel save(CustomerModel customer)
	{
		return customerRepository.save(customer);
	}
	
	public CustomerModel update(CustomerModel customer, CustomerVo customerVo )
	{
		customer.setCpfCnpj(!StringUtils.isEmpty(customerVo.getCpfCnpj())?customerVo.getCpfCnpj():customer.getCpfCnpj());
		customer.setName(!StringUtils.isEmpty(customerVo.getName())?customerVo.getName():customer.getName());
		return customerRepository.save(customer);
	}
	
	public void deleteById(Long id)
	{
		customerRepository.deleteById(id);
	}	
}
