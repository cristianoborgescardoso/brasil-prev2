package br.com.brasilprev.service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import br.com.brasilprev.model.ItemModel;
import br.com.brasilprev.model.vo.ItemVo;
import br.com.brasilprev.persistence.repository.ItemRepository;

@Service
public class ItemService {
	
	@Autowired
	private ItemRepository itemRepository;
	
	public List<ItemModel> getItems()
	{
		return itemRepository.findAll();
	}
	
	public Optional<ItemModel> getById(Long id)
	{
		return itemRepository.findById(id);
	}
	
	public Optional<ItemModel> getByPartNumber(String partNumber)
	{
		return itemRepository.findByPartNumber(partNumber);
	}
	
	public ItemModel save(ItemModel item)
	{
		return itemRepository.save(item);
	}
	
	public ItemModel update(ItemModel item, ItemModel newitem )
	{
		item.setDescription(!StringUtils.isEmpty(newitem.getDescription())?newitem.getDescription():item.getDescription());
		item.setPartNumber(!StringUtils.isEmpty(newitem.getPartNumber())?newitem.getPartNumber():item.getPartNumber());
		item.setPrice(!Objects.isNull(newitem.getPrice())?newitem.getPrice():item.getPrice());
		item.setItemId(!Objects.isNull(newitem.getItemId())?newitem.getItemId():item.getItemId());
		return itemRepository.save(item);
	}
	
	public void deleteById(Long id)
	{
		itemRepository.deleteById(id);
	}	
	
	

}
