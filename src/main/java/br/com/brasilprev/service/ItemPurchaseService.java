package br.com.brasilprev.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.brasilprev.model.ItemPurchaseModel;
import br.com.brasilprev.persistence.repository.ItemPurchaseRepository;

@Service
public class ItemPurchaseService {
	
	@Autowired
	private ItemPurchaseRepository itemPurchaseRepository;
	
	public Optional<ItemPurchaseModel> getByPurchaseIdAndItemId(Long purchaseId, Long itemId)
	{
		return itemPurchaseRepository.findByPurchase_purchaseIdAndItem_itemId(purchaseId, itemId);
	}

}
