package br.com.brasilprev.persistence.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.brasilprev.model.ItemPurchaseModel;

@Repository
public interface ItemPurchaseRepository extends JpaRepository<ItemPurchaseModel, Long>{	
	Optional<ItemPurchaseModel> findByPurchase_purchaseIdAndItem_itemId(Long purchaseId, Long itemId);
}

