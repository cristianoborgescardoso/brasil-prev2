package br.com.brasilprev.persistence.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.brasilprev.model.CustomerModel;
import br.com.brasilprev.model.ItemModel;

@Repository
public interface ItemRepository extends JpaRepository<ItemModel, Long>{
	
	Optional<ItemModel> findByPartNumber(String partNumber);

}	