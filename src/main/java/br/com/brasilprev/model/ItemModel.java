package br.com.brasilprev.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Table(name = "item")
@Entity
@Data
@JsonIgnoreProperties(value= {"itemsOrderList"})
public class ItemModel implements Serializable {

	private static final long serialVersionUID = 4739721709448323786L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "item_id")
	private long itemId;

	@Column(name = "price")
	private Float price;

	@Column(name = "description")
	private String description;

	@Column(name = "part_number")
	private String partNumber;

	@OneToMany(mappedBy = "item", targetEntity = ItemPurchaseModel.class, cascade = {CascadeType.PERSIST, CascadeType.MERGE },fetch = FetchType.LAZY)
	private List<ItemPurchaseModel> itemsOrderList;

	@Override
	public String toString() {
		return "ItemModel [itemId=" + itemId + ", price=" + price + ", description=" + description + ", partNumber="
				+ partNumber + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemModel other = (ItemModel) obj;
		if (partNumber == null) {
			if (other.partNumber != null)
				return false;
			else
			{
				return true;
			}
		} else if (!partNumber.equals(other.partNumber))
			return false;		
		if (itemId != other.itemId)
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + (int) (itemId ^ (itemId >>> 32));
		result = prime * result + ((partNumber == null) ? 0 : partNumber.hashCode());
		return result;
	}
	
}
