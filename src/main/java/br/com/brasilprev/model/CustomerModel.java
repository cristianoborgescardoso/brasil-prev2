package br.com.brasilprev.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Table(name = "customer")
@Entity
@Data
@JsonIgnoreProperties(value= {"purchasesList"})
public class CustomerModel implements Serializable {
	
	private static final long serialVersionUID = -6561822058036035435L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)	
	@Column(name = "customer_id")
	private long customerId;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "cpf_cnpj",unique = true)
	private String cpfCnpj;	
	
	@OneToMany(mappedBy = "customer", targetEntity = PurchaseModel.class,
    		cascade =  { CascadeType.PERSIST, CascadeType.MERGE },fetch = FetchType.LAZY)
	@JsonIgnore
	private List<PurchaseModel> purchasesList;

	@Override
	public String toString() {
		return "CustomerModel [customerId=" + customerId + ", name=" + name + ", cpfCnpj=" + cpfCnpj + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CustomerModel other = (CustomerModel) obj;
		if (cpfCnpj == null) {
			if (other.cpfCnpj != null)
				return false;
		} else if (!cpfCnpj.equals(other.cpfCnpj))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cpfCnpj == null) ? 0 : cpfCnpj.hashCode());
		return result;
	}	
}
