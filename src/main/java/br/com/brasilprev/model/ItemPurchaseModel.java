package br.com.brasilprev.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Table(name = "item_purchase")
@Entity
@Data
public class ItemPurchaseModel implements Serializable {

	private static final long serialVersionUID = -8710900948836857464L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)	
	@Column(name = "item_purchase_id")
	private long itemPurchaseId;	
	
	@Column(name = "amount")
	private Float amount;
	
	@Column(name = "current_price")
	private Float currentPrice;

	@ManyToOne()
	@JoinColumn(name = "item_id" )
	private ItemModel item;
	
	@ManyToOne()
	@JoinColumn(name = "purchase_id" )
	private PurchaseModel purchase;

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemPurchaseModel other = (ItemPurchaseModel) obj;
		if (item == null) {
			if (other.item != null)
				return false;
		} else if (!item.equals(other.item))
			return false;
		if (purchase == null) {
			if (other.purchase != null)
				return false;
		} else if (!purchase.equals(other.purchase))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((item == null) ? 0 : item.hashCode());
		result = prime * result + ((purchase == null) ? 0 : purchase.hashCode());
		return result;
	}
	
	
}
