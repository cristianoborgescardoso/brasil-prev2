package br.com.brasilprev.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.com.brasilprev.util.DateUtils;
import lombok.Data;

@Table(name = "purchase")
@Entity
@Data
public class PurchaseModel implements Serializable {	
	
	private static final long serialVersionUID = 2406117235000128349L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)	
	@Column(name = "purchase_id")
	private Long purchaseId;
	
	@Column(name = "purchaseTime")
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern=DateUtils.DATE_FORMAT_dd_MM_YYYY_HHmmss, timezone=DateUtils.TIME_ZONE)
	private Timestamp purchaseTime;
	
	@ManyToOne()
	@JoinColumn(name = "customer_id" )
	private CustomerModel customer;
	
	@OneToMany(mappedBy = "purchase", targetEntity = ItemPurchaseModel.class,
	    		cascade =  { CascadeType.PERSIST, CascadeType.MERGE })
	private List<ItemPurchaseModel> ItemsPurchaseList;

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PurchaseModel other = (PurchaseModel) obj;
		if (customer == null) {
			if (other.customer != null)
				return false;
		} else if (!customer.equals(other.customer))
			return false;
		if (purchaseId == null) {
			if (other.purchaseId != null)
				return false;
		} else if (!purchaseId.equals(other.purchaseId))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((customer == null) ? 0 : customer.hashCode());
		result = prime * result + ((purchaseId == null) ? 0 : purchaseId.hashCode());
		return result;
	}	
}
