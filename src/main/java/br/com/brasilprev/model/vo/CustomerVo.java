package br.com.brasilprev.model.vo;

import java.io.Serializable;

import br.com.brasilprev.model.CustomerModel;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CustomerVo implements Serializable{

	private static final long serialVersionUID = -5367414268310143476L;

	private Long customerId;
	
	private String name;
	
	private String cpfCnpj;

	public CustomerVo(CustomerModel customerModel) {
		this.customerId = customerModel.getCustomerId();
		this.name = customerModel.getName();
		this.cpfCnpj = customerModel.getCpfCnpj();
	}
}
