package br.com.brasilprev.model.vo;

import java.io.Serializable;

import br.com.brasilprev.model.ItemPurchaseModel;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ItemVo implements Serializable{
	
	private static final long serialVersionUID = 7210016700327973922L;

	private Long itemId;
	
	private Float price;
	
	private String description;	

	private String partNumber;
	
	private Float amount;
	
	private Float totalPrice;

	public ItemVo(ItemPurchaseModel itemPurchase)
	{
		this.itemId = itemPurchase.getItem().getItemId();
		this.price = itemPurchase.getCurrentPrice();
		this.description = itemPurchase.getItem().getDescription();
		this.partNumber = itemPurchase.getItem().getPartNumber();
		this.amount = itemPurchase.getAmount();
		this.totalPrice = itemPurchase.getCurrentPrice() * itemPurchase.getAmount();
	}	
}
