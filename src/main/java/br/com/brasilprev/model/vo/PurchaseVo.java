package br.com.brasilprev.model.vo;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.com.brasilprev.model.PurchaseModel;
import br.com.brasilprev.util.DateUtils;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PurchaseVo implements Serializable{
	
	private static final long serialVersionUID = -1268669172295831487L;

	private Long purchaseId;
	
	private CustomerVo customerVo;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern=DateUtils.DATE_FORMAT_dd_MM_YYYY_HHmmss, timezone=DateUtils.TIME_ZONE)
	private Timestamp purchaseTime;
	
 	private List<ItemVo> itemVoList;

	public PurchaseVo(PurchaseModel purchase) {
		this.purchaseId = purchase.getPurchaseId();
		this.customerVo = new CustomerVo(purchase.getCustomer());
		this.purchaseTime = purchase.getPurchaseTime();
		this.itemVoList = purchase.getItemsPurchaseList().stream().map(itemPurchaseModel -> new ItemVo(itemPurchaseModel))
				.collect(Collectors.toList());
	}
}
